object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Form3'
  ClientHeight = 303
  ClientWidth = 536
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object DBEdit1: TDBEdit
    Left = 80
    Top = 48
    Width = 121
    Height = 21
    TabOrder = 0
    OnChange = DBEdit1Change
  end
  object DBEdit2: TDBEdit
    Left = 80
    Top = 96
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object DBEdit3: TDBEdit
    Left = 80
    Top = 144
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object Button1: TButton
    Left = 88
    Top = 200
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 3
  end
  object Button2: TButton
    Left = 88
    Top = 240
    Width = 75
    Height = 25
    Caption = 'Button2'
    TabOrder = 4
  end
end
