object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 442
  ClientWidth = 997
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button4: TButton
    Left = 352
    Top = 46
    Width = 93
    Height = 25
    Caption = 'Mostrar Pokemon'
    TabOrder = 0
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 350
    Top = 100
    Width = 93
    Height = 25
    Caption = 'Inserir Pokemon'
    TabOrder = 1
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 350
    Top = 152
    Width = 95
    Height = 25
    Caption = 'Deletar Pokemon'
    TabOrder = 2
    OnClick = Button6Click
  end
  object DBLookupListBox1: TDBLookupListBox
    Left = 25
    Top = 8
    Width = 217
    Height = 251
    KeyField = 'id'
    ListField = 'nome'
    ListSource = DataModule2.DataSourcePokemon
    TabOrder = 3
  end
  object Panel1: TPanel
    Left = 480
    Top = 21
    Width = 249
    Height = 251
    TabOrder = 4
    object Label1: TLabel
      Left = 24
      Top = 37
      Width = 61
      Height = 13
      Caption = 'Identificador'
    end
    object Label2: TLabel
      Left = 24
      Top = 91
      Width = 46
      Height = 13
      Caption = 'Treinador'
    end
    object Label3: TLabel
      Left = 24
      Top = 139
      Width = 27
      Height = 13
      Caption = 'Nome'
    end
    object Label4: TLabel
      Left = 24
      Top = 184
      Width = 23
      Height = 13
      Caption = 'Nivel'
    end
    object DBText2: TDBText
      Left = 128
      Top = 88
      Width = 65
      Height = 17
      DataField = 'id_treinador'
      DataSource = DataModule2.DataSourcePokemon
    end
    object DBText3: TDBText
      Left = 128
      Top = 136
      Width = 65
      Height = 17
      DataField = 'nome'
      DataSource = DataModule2.DataSourcePokemon
    end
    object DBText4: TDBText
      Left = 128
      Top = 184
      Width = 65
      Height = 17
      DataField = 'nivel'
      DataSource = DataModule2.DataSourcePokemon
    end
    object DBText1: TDBText
      Left = 128
      Top = 37
      Width = 65
      Height = 17
      DataField = 'id'
      DataSource = DataModule2.DataSourcePokemon
    end
  end
end
