unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Unit2, Data.DB, Vcl.Grids, Vcl.DBGrids,
  Vcl.StdCtrls, Vcl.DBCtrls, Vcl.ExtCtrls;

type
  TForm1 = class(TForm)
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    DBLookupListBox1: TDBLookupListBox;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    DBText4: TDBText;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
    DataModule2.FDQueryTreinador.Open ();
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
    DataModule2.FDQueryTreinador.Delete();
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
   DataModule2.FDQueryTreinador.Append ();
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
    DataModule2.FDQueryPokemon.Open ();
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
    DataModule2.FDQueryPokemon.Append ();
end;

procedure TForm1.Button6Click(Sender: TObject);
begin
    DataModule2.FDQueryPokemon.Delete ();
end;

end.
