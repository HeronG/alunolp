object DataModule2: TDataModule2
  OldCreateOrder = False
  Height = 543
  Width = 740
  object FDPhysMySQLDriverLink1: TFDPhysMySQLDriverLink
    VendorLib = 'C:\EasyPHP-DevServer-14.1VC9\binaries\mysql\lib\libmysql.dll'
    Left = 80
    Top = 192
  end
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=joao352'
      'User_Name=root'
      'DriverID=MySQL')
    Connected = True
    Left = 208
    Top = 192
  end
  object DataSourcePokemon: TDataSource
    DataSet = FDQueryPokemon
    Left = 328
    Top = 192
  end
  object FDQueryPokemon: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'Select * from pokemon')
    Left = 440
    Top = 192
  end
  object DataSourceTreinador: TDataSource
    DataSet = FDQueryTreinador
    Left = 328
    Top = 288
  end
  object FDQueryTreinador: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'Select * from treinador')
    Left = 440
    Top = 288
  end
end
